const isValidTodo = require('./validators').isValidTodo;

test('Test correct todo, isValidTodo returns true', () => {
  expect(isValidTodo('My todo')).toBe(true);
});

test('Test null todo, isValidTodo returns false', () => {
  expect(isValidTodo(null)).toBe(false);
});

test('Test undefined todo, isValidTodo returns false', () => {
  expect(isValidTodo(undefined)).toBe(false);
});

test('Test empty todo, isValidTodo returns false', () => {
  expect(isValidTodo('')).toBe(false);
});
