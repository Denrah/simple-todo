// eslint-disable-next-line
const isValidTodo = function (text) {
  return text !== null && text !== undefined && text !== '';
};

module.exports.isValidTodo = isValidTodo;